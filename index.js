const data = require('./data')

// - 1. Write a function called `countAllPeople` which counts the total number of
//  people in `got` variable defined in `data.js` file.

const countAllPeople = data => {
  let totalcnt = 0
  for (const house of data.houses) {
    const peopleByHouse = house.people.reduce((cnt, hname) => {
      if (hname.name) {
        return cnt + 1
      }
    }, 0)
    totalcnt += peopleByHouse
  }

  return totalcnt
}
// console.log('count of all people', countAllPeople(data))

// - 2. Write a function called `peopleByHouses` which counts the total number of people in
//  different houses in the `got` variable defined in `data.js` file.

function peopleByHouses (data) {
  let output = {}
  for (const house of data.houses) {
    const tempcnt = house.people.reduce((cnt, hname) => {
      if (hname.name) {
        return cnt + 1
      }
    }, 0)
    output[house.name] = tempcnt
  }

  return output
}
// console.log(`count of people with respect to houses`, peopleByHouses(data))

// - 3. Write a function called `everyone` which returns a array of
//  names of all the people in `got` variable.

function everyone (data) {
  let allnames = []
  for (const house of data.houses) {
    const output = house.people.filter(hname => {
      if (hname.name) {
        allnames.push(hname.name)
      }
    })
  }

  return allnames
}
// console.log(everyone(data))

// - 4. Write a function called `nameWithS` which returns a array of names of all
//  the people in `got` variable whose name includes `s` or `S`.

const nameWithS = (data, everyone) => {
  let nameStartwithS = []
  const allnames = everyone(data)
  const snames = allnames.filter(sname => {
    if (sname[0] === 's' || sname[0] === 'S') {
      nameStartwithS.push(sname)
    }
  })

  return nameStartwithS
}

// console.log(nameWithS(data, everyone))

// - 5. Write a function called `nameWithA` which returns a array of names of all the people
// in `got` variable whose name includes `a` or `A`.

const nameWithA = (data, everyone) => {
  let nameStartwithA = []
  const allnames = everyone(data)
  const snames = allnames.filter(sname => {
    if (sname[0] === 'a' || sname[0] === 'A') {
      nameStartwithA.push(sname)
    }
  })

  return nameStartwithA
}

// console.log(nameWithA(data, everyone))

// - 6. Write a function called `surnameWithS` which returns a array of names of all the people
//  in `got` variable whoes surname is starting with `S`(capital s).

const surnameWithS = (data, everyone) => {
  let surnamewiths = []
  const allnames = everyone(data)

  const surname = allnames.filter(name => {
    const surname = name.split(' ')
    if (surname[1][0] === 'S') {
      surnamewiths.push(name)
    }
  })

  return surnamewiths
}

// console.log(surnameWithS(data, everyone))

// - 7. Write a function called `surnameWithA` which returns a array of names of all the people
// in `got` variable whoes surname is starting with `A`(capital a).

const surnameWithA = (data, everyone) => {
  let surnamewitha = []
  const allnames = everyone(data)

  const surname = allnames.filter(name => {
    const surname = name.split(' ')
    if (surname[1][0] === 'A') {
      surnamewitha.push(name)
    }
  })

  return surnamewitha
}

// console.log(surnameWithA(data, everyone))

// - 8. Write a function called `peopleNameOfAllHouses` which returns an object with the key
// of the name of house and value will be all the people in the house in an array.

const peopleNameOfAllHouses = data => {
  const resultantObject = {}
  for (const house of data.houses) {
    const namesList = house.people.reduce((names, people) => {
      names.push(people.name)
      return names
    }, [])
    resultantObject[house.name] = namesList
  }

  return resultantObject
}

console.log(peopleNameOfAllHouses(data))
